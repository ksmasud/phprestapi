<?php

header("content-type: application/json");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Headers: application/json");

class Mrlmath {
	public $number1;
    public $number2;
	public $operator;

	public function __construct($number1, $number2, $operator) {
		$this->number1 = $number1;
		$this->number2 = $number2;
		$this->operator = $operator;
	}

	public function calcMethod() {
		switch ($this->operator) {
			case '+':
				$result = $this->number1 + $this->number2;
				break;
			case '-':
				$result = $this->number1 - $this->number2;
				break;
			case '*':
				$result = $this->number1 * $this->number2;
				break;
            case '/':
               $result = $this->number1 / $this->number2;
                break;
			default:
				$result = "Sorry, you entered an unknown operator: $this->operator";
				break;
		}
		return $result;
	}
}

$results = '';
$method = $_SERVER['REQUEST_METHOD'];
if ($method === 'POST') {
	     $number1 = $_POST['number1'];   
		 if (!is_numeric($number1)) 
		 echo "Please enter a number only at number1.";
		 $number2 = $_POST['number2'];
		 if (!is_numeric($number2)) 
		 echo "Please enter a number only at number2.";
         $operator = $_POST['operator'];  
	     $calculation = new Mrlmath($number1, $number2, $operator);
         $results = $calculation->calcMethod();
 }
 
echo json_encode($results);